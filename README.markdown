# mirror_git_repos

This keeps a set of git repos up to date with their corresponding upstreams. It was written with Puppet modules in mind, so that your Puppet infrastructure can deploy modules entirely from a local git server for speed, resiliency, and security reasons.

For TFS servers it can create the repo if it does not already exist.
This has been tested pushing to TFS 2017 (on prem).

Protocols. Uses:
 - whatever protocol for git is specified in the source URL to fetch
   from the source repo
 - http to create the repo on TFS
 - git over ssh to push the repo to TFS

Relevant docs:
- https://docs.microsoft.com/en-us/azure/devops/integrate/previous-apis/git/repositories
- https://docs.microsoft.com/en-us/azure/devops/integrate/get-started/authentication/pats?view=vsts

## Usage

Create a config file that contains just a list of URLs to remote git repos for which you want to mirror locally, one URL per line, eg:

```
https://github.com/puppetlabs/puppetlabs-stdlib.git
https://github.com/jessereynolds/puppet-pe_nc_backup.git
```

Now run the script with ruby and pass the path to the above file as an argument:

```
ruby bin/mirror_git_repos.rb path/to/urls_file.txt
```

Note that no ruby gems are needed for this script, all dependencies exist in ruby or its stdlib.

## To Do

- Get this supporting generic git servers. It's only the repo creation steps that are specific to the git server in use (currently only supports TFS for this.) and this is a nicety really.


