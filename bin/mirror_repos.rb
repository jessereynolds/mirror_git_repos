#!/usr/bin/env ruby

require 'fileutils'

def execute_this(cmd, dir = Dir.pwd, options = {})
  Dir.chdir(dir) do
    unless system(cmd)
      if options[:continue_on_error]
        puts "Warning: the following command exited non-zero: #{cmd}"
      else
        raise "Error executing command (non-zero exit status): #{cmd}"
      end
    end
  end
end

if ARGV.empty?
  puts "No file specified so reading URLs from stdin... ctrl-d to end."
end

# https://github.com/puppetlabs/puppetlabs-inifile.git
# => git@tfs_hostname:project/_git/puppetlabs-puppetlabs-inifile

mirror_dir = 'git_mirrors'
FileUtils.mkdir_p mirror_dir

git_host = 'tfs.example'
path = 'tfs/example'
project = 'puppet-control-repo'

ARGF.each_with_index do |line, index|
  filename = ARGF.filename
  source_url = line.chomp
  slash_parts = line.chomp.split('/')
  owner = slash_parts[-2]
  name  = slash_parts[-1].gsub(/\.git$/, '')
  puts "==> #{owner}/#{name}"

  name_new = "#{owner}-#{name}"
  git_dir  = File.join(mirror_dir, name_new)

  # tfs git paths are a little unconventional...
  target_url = "git@#{git_host}:#{path}/#{project}/_git/#{name_new}"

  if File.exists?(git_dir)
    puts "===> Updating existing mirror at #{git_dir}"
    cmd = "git fetch --prune"
    execute_this(cmd, git_dir)
  else
    puts "===> Setting up new mirror at #{git_dir}"
    cmd = "git clone --mirror '#{source_url}' '#{git_dir}'"
    execute_this(cmd)

    puts "===> Adding git remote 'mirror' #{target_url}"
    cmd = "git remote add mirror #{target_url}"
    execute_this(cmd, git_dir)
  end

  puts "===> Pushing to mirror"
  cmd = "git push --mirror mirror"
  execute_this(cmd, git_dir, :continue_on_error => true)

end
