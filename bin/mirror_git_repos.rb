#!/usr/bin/env ruby

# Mirror git repos
# Author: Jesse.Reynolds@puppet.com (2017, 2018)

# For TFS servers it can create the repo if it does not already exist.
# This has been tested pushing to TFS 2017 (on prem).

# Protocols. Uses:
# - whatever protocol for git is specified in the source URL to fetch
#   from the source repo
# - http to create the repo on TFS
# - git over ssh to push the repo to TFS

# Relevant docs:
# - https://docs.microsoft.com/en-us/azure/devops/integrate/previous-apis/git/repositories
# - https://docs.microsoft.com/en-us/azure/devops/integrate/get-started/authentication/pats?view=vsts

require 'net/http'
require 'openssl'
require 'json'
require 'fileutils'

# Configuration:

git_host   = 'tfs.example'
git_type   = 'tfs'
path       = 'tfs/DefaultCollection'
project    = 'Puppet%20Mirrors'
username   = "someuser"
token      = "tokentokentoken"
mirror_dir = 'git_mirrors'

class GitMirror

  def self.execute_this(cmd, dir = Dir.pwd, options = {})
    Dir.chdir(dir) do
      unless system(cmd)
        if options[:continue_on_error]
          puts "Warning: the following command exited non-zero: #{cmd}"
        else
          raise "Error executing command (non-zero exit status): #{cmd}"
        end
      end
    end
  end

  def self.repo_exists?(name_new, config)

    uri = URI("#{config[:project_repos_api]}/#{name_new}?api-version=1.0")

    Net::HTTP.start(uri.host, uri.port,
      :use_ssl => uri.scheme == 'https') do |http|

      request = Net::HTTP::Get.new uri.request_uri
      request.basic_auth config[:username], config[:token]
      response = http.request request

      case response
      when Net::HTTPSuccess # 200 OK - repo exists
        return true
      when Net::HTTPNotFound # 404 - repo does not exist
        return false
      else
        raise "Error when getting repository status for #{name_new}: response: #{response}"
      end

    end
  end

  def self.create_repo(name_new, config)
    project_repos_api = "http://#{config[:git_host]}/tfs/DefaultCollection/#{config[:project]}/_apis/git/repositories"

    uri = URI("#{project_repos_api}/#{name_new}?api-version=1.0")
    data = {
      "name" => name_new,
    }

    Net::HTTP.start(uri.hostname, uri.port) {|http|
      request = Net::HTTP::Post.new uri
      request.content_type = "application/json"
      request.body = data.to_json
      request.basic_auth config[:username], config[:token]

      response = http.request request
      unless response.is_a?(Net::HTTPSuccess)
        raise "Error when creating repository #{name_new}: response: #{response}"
      end
    }
  end
end # Class GitMirror


config = {
  :git_host          => git_host,
  :git_type          => git_type,
  :project           => project,
  :username          => username,
  :token             => token,
  :project_repos_api => "http://#{git_host}/#{path}/#{project}/_apis/git/repositories"
}

FileUtils.mkdir_p mirror_dir

if ARGV.empty?
  puts "No file specified so reading URLs from stdin... ctrl-d to end."
end

ARGF.each_with_index do |line, index|
  # Each line of the input (or source file) is the source URL of a repo
  # that we will be mirroring
  source_url = line.chomp

  slash_parts = source_url.split('/')
  owner       = slash_parts[-2]
  name        = slash_parts[-1].gsub(/\.git$/, '')

  puts "==> #{owner}/#{name}"

  name_new = "#{owner}-#{name}"
  git_dir  = File.join(mirror_dir, name_new)

  # determine the url of the target git URL on the TFS server:
  target_url = "git@#{git_host}:#{path}/#{project}/_git/#{name_new}"

  # create the repo on TFS if it does not already exist:
  # if GitMirror.repo_exists?(name_new, config)
  #   puts "===> Repo #{name_new} already exists at destination."
  # else
  #   print "===> Repo #{name_new} does not exist at destination, creating... "
  #   GitMirror.create_repo(name_new, config)
  #   puts "Done."
  # end

  if File.exists?(git_dir)
    puts "===> Updating existing mirror at #{git_dir}"
    cmd = "git fetch --prune"
    GitMirror.execute_this(cmd, git_dir)
  else
    puts "===> Setting up new mirror at #{git_dir}"
    cmd = "git clone --mirror '#{source_url}' '#{git_dir}'"
    GitMirror.execute_this(cmd)

    puts "===> Adding git remote 'mirror' #{target_url}"
    cmd = "git remote add mirror #{target_url}"
    GitMirror.execute_this(cmd, git_dir)
  end

  puts "===> Pushing to mirror"
  cmd = "git push --mirror mirror"
  GitMirror.execute_this(cmd, git_dir, :continue_on_error => true)

end
